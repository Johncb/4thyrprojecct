class CreateHands < ActiveRecord::Migration
  def change
    create_table :hands do |t|
      t.integer :winning , :default => false, :null => false
      t.string :cards
      t.string :best_hand
      t.string :rank
      t.string :hole_cards, :null => false
      t.integer :user_id
      t.integer :game_id
      t.timestamps
    end
  end
end
