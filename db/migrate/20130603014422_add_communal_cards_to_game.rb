class AddCommunalCardsToGame < ActiveRecord::Migration
  def change
    add_column :games, :communal_cards, :string
  end
end
