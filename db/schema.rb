# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130603232245) do


  create_table "games", :force => true do |t|
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "state"
    t.integer  "pot"
    t.integer  "bet"
    t.integer  "winner"
    t.integer  "activeuser"
    t.string   "choice"
    t.integer  "previous_bet"
    t.string   "previous_choice"
    t.string   "communal_cards"
    t.string   "deck"
  end

  create_table "hands", :force => true do |t|
    t.integer  "winning",    :default => 0, :null => false
    t.string   "cards"
    t.string   "best_hand"
    t.string   "rank"
    t.string   "hole_cards"
    t.integer  "user_id"
    t.integer  "game_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "state"
  end

  create_table "users", :force => true do |t|
    t.string   "username",                                                :null => false
    t.string   "first_name",                                              :null => false
    t.string   "last_name",                                               :null => false
    t.date     "dob",                                                     :null => false
    t.float    "balance",                              :default => 100.0, :null => false
    t.datetime "created_at",                                              :null => false
    t.datetime "updated_at",                                              :null => false
    t.string   "password",               :limit => 60
    t.string   "email",                                :default => "",    :null => false
    t.string   "encrypted_password",                   :default => "",    :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                        :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "game_id"
    t.integer  "ready"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
