Project.Views.User =  Backbone.View.extend({
	template: JST['user'],

	events: {
    'click .button': 'buttonClicked'
  	},


  	initialize: function(model,game,container,game_id,hand){
  		_.bindAll(this, 'render');
  		//this.model.on('change', this.render, this);
  		this.game = game;
  		this.hand = hand;
  		this.game_id = game_id;
  		this.container = container;
  		$(container).html(this.render().el);
  	},

  	
	render: function() {
		console.log("user rendered from user");
	   	$(this.el).html(this.template({model: this.model}));

	   	this.renderHand();
	    return this;
	},

	renderHand: function(){
		if(this.game.get('state') !== 'waiting'){
			var hand_view = new Project.Views.Hand({model: this.hand},container);
			$('#hand').html(hand_view.render().el);
		}
	},
// adds buttons to the rendered view
	addButtons: function(){
		$(this.container + " #button_group ul").append('<li><a class="button" id="fold_button' + this.game_id +'">fold</a></li>')
		$(this.container + " #button_group ul").append('<li><a class="button" id="check_button' + this.game_id +'">check</a></li>')
		$(this.container + " #button_group ul").append('<li><a class="button" id="call_button' + this.game_id +'">call</a></li>')
		$(this.container + " #button_group ul").append('<li><a class="button" id="bet_button' + this.game_id +'">bet</a></li>')
		$(this.container + " #button_group ul").append('<li><a class="button" id="raise_button' + this.game_id +'">raise</a></li>')
	},


//sets the available button options for the user
	availableOptions: function(){
		console.log("available actions called");
		this.activeUser();
		if (this.active && (this.game.get('choice') == null || this.game.get('choice') == 'check' || this.game.get('choice')== "first_user_choice")){
			this.activateButtonsForCheck();
			console.log("went into the bracket for active user, choice check,null,or first choice")
		}
		else if(this.active && (this.game.get('choice') == 'bet' || this.game.get('choice') == 'raise')){
			console.log("went into the bracket for active user, bet raise");
			$("#fold_button" + this.game_id).removeClass('button disabled');
			$("#call_button" + this.game_id).removeClass('button disabled');
			$("#raise_button" + this.game_id).removeClass('button disabled');
			$("#fold_button" + this.game_id).addClass('button');
			$("#call_button" + this.game_id).addClass('button');
			$("#raise_button" + this.game_id).addClass('button');
		}
		else {
			console.log("went into non active user bracket");
			this.deactivateButtons();
		}
	},
// determines if this user is the active user or not 
	activeUser: function(){
		active = this.game.get('activeuser');
		//alert(this.game.get('activeuser') + "---" +this.model.get('id') );	
		if (this.game.get('activeuser') === this.model.get('id')){
			this.active = true;
			console.log("this user "  + this.model.get('id') + " is the active user");
		}
		else{
			this.active = false;
			console.log("this user  "  + this.model.get('id') + " is NOT the active user");
		}
	},
// click event handler
	buttonClicked: function(e){
  		var buttonClicked =$(e.currentTarget).attr('id');
  		if (buttonClicked == "check_button" +  this.game_id){
  			this.check();
  		}
  		else if (buttonClicked == "fold_button" + this.game_id){
  			this.fold();
  		}
  		else if (buttonClicked == "call_button" + this.game_id){
  			this.callButton();
  		}
  		else if (buttonClicked == "bet_button" + this.game_id){
  			this.bet();
  		}
  		else if (buttonClicked == "raise_button" + this.game_id){
  			this.raise();
  		}
  	},
// to be executed when check button is called sets the choice on the game.
	check: function(){
		var class_type = $('#check_button'+this.game_id).attr('class');
		if(class_type === 'button'){
  			this.game.set('choice','check');
  			this.game.save();
  			console.log("check button pressed choice is: " + this.game.get('choice') + " ")
			this.deactivateButtons();
		}
  	},
// to be executed when bet button is called  takes the amount the user wishes to bet in a dialog
  	bet: function(){
  		var class_type = $('#bet_button'+this.game_id).attr('class');
  		if(class_type === 'button'){
			var amount=prompt("Please enter your bet",10);
			if (amount !== null){
				this.game.set({bet: amount, choice: 'bet'});
				this.game.save();
				console.log("bet button pressed choice is: " + this.game.get('choice') + " bet is: " + this.game.get('bet'));
				this.deactivateButtons();
			}
		}
  	},
// to be executed when raise button is called takes the amount the user wishes to bet in a dialog
	raise: function(){
		var class_type = $('#raise_button'+this.game_id).attr('class');
  		if(class_type === 'button'){
			var amount=prompt("Please enter your raise",10);
			if (amount !== null){
				this.game.set({bet: amount, choice: 'raise'});
				this.game.save();
				console.log("bet button pressed choice is: " + this.game.get('choice') + " bet is: " + this.game.get('bet'));
				this.deactivateButtons();
			}
		}
	},
// executed if fold but is called confirmed with a dialog box
	fold: function(){
		var class_type = $('#fold_button'+this.game_id).attr('class');
  		if(class_type === 'button'){
			var fold=confirm("are you sure you want to fold")
			if (fold == true){
				this.game.set('choice', 'fold');
				this.game.save();
				console.log("raise button pressed choice is: " + this.game.get('choice') + " bet is: " + this.game.get('bet'));
				this.deactivateButtons();
			}
		}
	},

// executed if call button is called name clash with backbone.call method renamed callbutton
	callButton: function(){
		var class_type = $('#call_button'+this.game_id).attr('class');
  		if(class_type === 'button'){
			this.game.set('choice', 'call')
			this.game.save();
			console.log("bet button pressed choice is: " + this.game.get('choice'));
			this.deactivateButtons();
		}
	},
// activates available buttons to the user
	activateButtonsForCheck: function(){
		console.log("activateButtons called");
		$("#fold_button" + this.game_id).removeClass('button disabled');
		$("#raise_button" + this.game_id).removeClass('button disabled');
		//$("#call_button" + this.game_id).removeClass('button disabled');
		$("#bet_button" + this.game_id).removeClass('button disabled');
		$("#check_button" + this.game_id).removeClass('button disabled');
		$("#fold_button" + this.game_id).addClass('button');
		$("#raise_button" + this.game_id).addClass('button');
		$("#bet_button" + this.game_id).addClass('button');
		$("#check_button" + this.game_id).addClass('button');
	},
// deactivates unavailable buttons to the user
	deactivateButtons: function(){
		console.log("deactivateButtons called")
		$("#fold_button" + this.game_id).removeClass('button');
		$("#raise_button" + this.game_id).removeClass('button');
		$("#call_button" + this.game_id).removeClass('button');
		$("#bet_button" + this.game_id).removeClass('button');
		$("#check_button" + this.game_id).removeClass('button');
		$("#fold_button" + this.game_id).addClass('button disabled');
		$("#raise_button" + this.game_id).addClass('button disabled');
		$("#call_button" + this.game_id).addClass('button disabled');
		$("#bet_button" + this.game_id).addClass('button disabled');
		$("#check_button" + this.game_id).addClass('button disabled');
	},
	// toggles buttons on/off
	fadeButtons: function(){
 		$("#fold_button").toggleClass('button button disabled');
 		$("#raise_button").toggleClass('button button disabled');
 		$("#call_button").toggleClass('button button disabled');
		$("#check").toggleClass('button button disabled');
		$("#bet_button").toggleClass('button button disabled');
		$("#check_button").toggleClass('button button disabled');
 	}
});