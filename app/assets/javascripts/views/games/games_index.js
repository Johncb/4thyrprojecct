Project.Views.GamesIndex =  Backbone.View.extend({
	template: JST['games/index'],
  	
  	initialize: function() {
  		_.bindAll(this, 'render');
      	return this.collection.on('reset', this.render);
  	},

  	render: function() {
    	$(this.el).html(this.template({games: this.collection}));
    	return this;
  	},

    options: function(){
    }
});