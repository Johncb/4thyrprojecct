class User < ActiveRecord::Base
  has_many :hands
  belongs_to :game
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable

  # Setup accessible (or protected) attributes for your model
    attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :first_name, :last_name,:dob,:game_id
    
    email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    
    validates :username, :presence => true, :uniqueness => true
    validates :first_name, :presence => true
    validates :last_name, :presence => true
    validates_date :dob, :presence => true, :on_or_before => lambda {Date.today.years_ago(18)},
    :on_or_before_message => "must be at least 18 years old to join this site"
    validates :password, length: { in: 8..128 }, on: :create
    validates :password, length: { in: 8..128 }, on: :update, allow_blank: true
    validates_confirmation_of :password
    validates_numericality_of :balance, :greater_than_or_equal_to => 0
   # validate :passwords_equal
    validates :email, :presence => true, :uniqueness => true ,
                      :format   => { :with => email_regex }

    def top_up
      if self.balance < 1000
        self.balance = 1000
        self.save
      end
      return self.balance
    end

    # def bet
    #   self.balance = self.balance - 5;
    # end
end