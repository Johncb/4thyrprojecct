class Deck

	def initialize()
		@pokerdeck = []
        @communal_cards = []
        make_deck
    end

    def shuffle
    	@pokerdeck.shuffle!
        @pokerdeck.shuffle!
        @pokerdeck.shuffle!
    end

    def deal_card
    	@pokerdeck.pop
    end

    def deal_flop
        flop = @pokerdeck.pop(3)
        @communal_cards = flop
        return flop
    end

    def deal_turn
        turn = @pokerdeck.pop
        @communal_cards << turn
        turn
    end

    def deal_river
        river = @pokerdeck.pop
        @communal_cards << river
        river
    end

    def communal_cards
    	@communal_cards
    end

    def make_deck
        #club spade heart and diamond
        suits = ["c","s","h","d"]
        suits.each do |s|
            (2..14).each do |i|
                suit = s
                number = i
                card = Card.new(number,suit)
                @pokerdeck << card
            end
        end
    end

end
