class Hand < ActiveRecord::Base
	belongs_to :game
	belongs_to :user
	#has_many :cards
	#regex to turn hand strings into card numbers 
	# /\D*(\d),\"suit\":\"(.)/
	serialize :hole_cards, Array
	serialize :cards, Array
	serialize :best_hand, Array
	after_initialize :default_values

	 def default_values()
		 self.state = 0
	 end  

	def add_hole_card card
		self.hole_cards << card
		self.save
	end

	def add_card= card
		self.cards << card
		self.save
	end

	def add_communal_cards= communal_cards
		self.cards = self.hole_cards + communal_cards
		self.save
	end

end