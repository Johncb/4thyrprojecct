class GamesController < ApplicationController

    before_filter :authenticate_user! 
    
    
    def index
        # @user = User.find_by_id(current_user.id)
        # @user.game_id = 0
        # @user.save
        @games = Game.all
        respond_to do |format|
            format.html # new.html.erb
            format.json { render json: @games }
        end
    end

    #/get  /games/1/hands.json
    def get_current_hands
        @game = Game.find(params[:id])
        hands = @game.hands.select {|hand| hand.state == 0}
        respond_to do |format|
            format.json {render json: hands}
        end
    end

    def get_communal_cards
        @game = Game.find(params[:id])
        cards = @game.communal_cards
        respond_to do |format|
            format.json { render json: cards}
        end
    end
  
    # GET /Games/1
    # GET /Games/1.json

    # GET /Games/new
    # GET /Games/new.json
    def new
        @game = Game.new
    
        respond_to do |format|
            format.html # new.html.erb
            format.json { render json: @game }
        end
    end
  
    # GET /Games/1/edit
    def edit
      @game = Game.find(params[:id])
    end
  
    # POST /Games
    # POST /Games.json
    def create
        @game = Game.new()
    
        respond_to do |format|
            if @game.save
                format.html { redirect_to @game, notice: 'Game was successfully created.' }
                format.json { render json: @game, status: :created, location: @game }
            else
                format.html { render action: "new" }
                format.json { render json: @game.errors, status: :unprocessable_entity }
            end
        end
    end
  
    # PUT /Games/1
    # PUT /Games/1.json
    def update
        @game = Game.find(params[:id])
        unless params[:choice].nil? or params[:choice] == "first_user_choice"
           @game.choice = params[:choice]
            @game.activeuser = params[:activeuser]
           @game.state = params[:state]
           @game.save
            @game.user_choice
           params[:choice] = @game.choice
          params[:activeuser] = @game.activeuser
        end
        respond_to do |format|
            if @game.update_attributes(params[:Game])
                format.html { redirect_to @game, notice: 'Game was successfully updated.' }
                format.json { head :no_content }
            else
                format.html { render action: "edit" }
                format.json { render json: @game.errors, status: :unprocessable_entity }
            end
        end
        
		
        
    end
  
    # DELETE /Games/1
    # DELETE /Games/1.json
    def destroy
        @game.destroy
    
        respond_to do |format|
            format.html { redirect_to Games_url }
            format.json { head :no_content }
        end
    end

    def add_player
        @game.add_player(current_user);
    end

    def play_hand
        @game = Game.find_by_id(params[:id])
        @game.play_hand
        @game.save
    end



    def show
        
       @game = Game.find(params[:id])
        respond_to do |format|
            format.json { render json: @game }
        end
    end
end
