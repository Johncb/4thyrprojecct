Given /^I Have (.*) users$/ do |number|
	i = 1
	number = number.to_i
	#need to populate database
	while(i <= number)
		user = User.find_by_id(i)
		@table.users << user 
		i = i + 1
	end 
end

Given /^I have a game$/ do
  @table = Game.new()
end

Given /^State is "(.*?)"$/ do |state|
	@table.state.should == state
end

When /^I change State (.) times$/ do |times|
	i = 1
	number = number.to_i
	times_number = times.to_i
	while(i <= times_number)
		@table.change_state
		i = i + 1
	end
end

When /^the users are dealt the following cards$/ do |card_table|
	array,cards,number_suit_array = [],[],[]
	array = card_table.raw.flatten!
	array.each do |number_suit|
		number_suit_array = number_suit.split(",")
		card = Card.new(number_suit_array[0].to_i,number_suit_array[1])
		cards << card
	end
	@table.save
  	@table.users.each do |player|
  		hand = Hand.new()
		hand.game_id = @table.id
		hand.user_id = player.id
		hand.add_hole_card cards.pop()
		hand.add_hole_card cards.pop()
		@table.hands << hand
		@table.save
		hand.save
	end
end

When /^the flop turn and river are$/ do |table_of_communal_cards|
	array,@communal_cards,number_suit_array = [],[],[]	
	array = table_of_communal_cards.raw.flatten!
	array.pop
	array.each do |number_suit|
		number_suit_array = number_suit.split(",")
		card = Card.new(number_suit_array[0].to_i,number_suit_array[1])
		@communal_cards << card
	end
	@communal_cards.length.should == 5
end

Then /^State should be "(.*?)"$/ do |state|
	@table.state.should == state
end


Then /^the winning hand should be$/ do |table|
	expected_cards, actual_cards = [],[],[]	
	expected_cards = table.raw.flatten!
	expected_cards.pop
	winning_player = @table.users.select { |user| user.id == @table.winner}[0]
	hand = winning_player.hands.last
	hand.best_hand.each do |card|
		value = "#{card.number}," + card.suit
		actual_cards << value
	end
	@table.save	
	actual_cards.sort! {|x,y| y <=> x}
	expected_cards.sort! {|x,y| y <=> x}
	actual_cards.should == expected_cards
end

Then /^the hand rank should be "(.*?)"$/ do |rank|
	hands = @table.hands.select { |hand| hand.state == 0 }
	hands.each do |hand|
			hand.add_communal_cards= @communal_cards
			hand.save
		end
	 	hand_evaluator = HandEvaluator.new(@table.users,@table)
	@table.winner = hand_evaluator.determine_winner
	winning_player = @table.users.select { |user| user.id == @table.winner}[0]
	winning_hand = winning_player.hands.last
  	rank.should == winning_hand.rank
end

Then /^the winning hand should be (.*) the database$/ do |hand|
	hand = Hand.find_by_user_id_and_game_id(@table.winning_player.id, @table.id)
	hand.should == @table.winning_player.hand
end


